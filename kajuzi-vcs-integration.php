<?php 
/*
Plugin Name: Kajuzi VCS Integration
Plugin URI: http://kajuzi.com/work/wordpress/kajuzi-vcs-integration
Description: Custom plugin for displaying VCS payment a form on a page. Use short code [kvi_vcs_form] to display the form anywhere inside content
Version: 0.0.3
Author: Kajuzi
Author URI: http://kajuzi.com/c/kvi
Text Domain: kvi
*/


if ( ! defined( 'WPINC' ) ) {die;}

// Initialize
if ( file_exists( plugin_dir_path( __FILE__ ) . 'core-init.php' ) ) {
require_once( plugin_dir_path( __FILE__ ) . 'core-init.php' );
}