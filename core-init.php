<?php 

if ( ! defined( 'WPINC' ) ) {die;}
// Define Constants
define('KVI_CORE_INC',dirname( __FILE__ ).'/includes/');
define('KVI_CORE_IMG',plugins_url( 'assets/img/', __FILE__ ));
define('KVI_CORE_CSS',plugins_url( 'assets/css/', __FILE__ ));
define('KVI_CORE_JS',plugins_url( 'assets/js/', __FILE__ ));
define('KVI_PLUGIN_BASENAME',plugin_basename( 'kajuzi-vcs-integration/kajuzi-vcs-integration.php' ));
define('KVI_PLUGIN_NAME',plugin_basename( 'kajuzi-vcs-integration' ));
define('KVI_VCS_MERCHANT_ID', get_option( 'kvi_vcs_merchant_id' ));
define('KVI_CURRENCYLAYER_API_KEY', get_option( 'kvi_currencylayer_api_key' ));
define('KVI_EXCHANGE_RATE_API_ENDPOINT', 'http://apilayer.net/api/live?access_key='.KVI_CURRENCYLAYER_API_KEY.'&currencies=USD,BWP,ZAR&format=1');

/*
*  Register CSS
*/
function kvi_register_core_css(){
wp_enqueue_style('kvi-core', KVI_CORE_CSS . 'kvi-core.css',null,time('s'),'all');
};
add_action( 'wp_enqueue_scripts', 'kvi_register_core_css' );    
/*
*
*  Register JS/Jquery Ready
*
*/
function kvi_register_core_js(){
// Register Core Plugin JS	
wp_enqueue_script('kvi-core', KVI_CORE_JS . 'kvi-core.js','jquery',time(),true);
};
add_action( 'wp_enqueue_scripts', 'kvi_register_core_js' );    
/*
*
*  Includes
*
*/ 
// Load the Functions
if ( file_exists( KVI_CORE_INC . 'kvi-core-functions.php' ) ) {
	require_once KVI_CORE_INC . 'kvi-core-functions.php';
}     
// Load the ajax Request
if ( file_exists( KVI_CORE_INC . 'kvi-ajax-request.php' ) ) {
	require_once KVI_CORE_INC . 'kvi-ajax-request.php';
} 
// Load the Shortcodes
if ( file_exists( KVI_CORE_INC . 'kvi-shortcodes.php' ) ) {
	require_once KVI_CORE_INC . 'kvi-shortcodes.php';
}