<?php 
/*
*	Shortcodes
*/

if ( ! defined( 'WPINC' ) ) {die;}
/*
*  Display anywhere using shortcode: [kvi_vcs_form]
*/

add_action( 'init', 'kvi_register_shortcodes');

function kvi_vcs_form_display($atts, $content = NULL){
        global $wp;

		extract(shortcode_atts(array(
      	'el_class' => '',
      	'el_id' => '',	
        ),$atts));

        $current_url = home_url( add_query_arg( array(), $wp->request ) );
        $approved_url = (get_option('kvi_approved_url') != '')? : $current_url;
        $declined_url = (get_option('kvi_declined_url') != '')? : $current_url;
        
        if (get_option('kvi_vcs_merchant_id') && get_option('kvi_currencylayer_api_key')) {
            $out =  '<form id="kvi-vcs-payment-form" class="form-horizontal" role="form" action="https://www.vcs.co.za/vvonline/vcspay.aspx" method="POST">
                <div class="col-md-6"><label class="control-label">Name On Card *</label>
                <input class="form-control" name="CardholderName" type="text" /></div>
                <div class="col-md-6"><label class="control-label ">Phone (optional)</label>
                <input class="form-control" name="p8" type="text" /></div>
                <div class="col-md-6"><label class="control-label">Email *</label>
                <input class="form-control" name="CardholderEmail" type="email" /></div>
                <div class="col-md-6"><label class="control-label">Booking Reference *</label>
                <input class="form-control" name="p2" type="text" /></div>
                <div class="col-md-6"><label class="control-label">Amount *</label>
                <input id="amount" class="form-control" type="number" /></div>
                <div class="col-md-6"><label class="control-label">Currency *</label>
                <select id="currency" class="form-control" name="m_5">
                <option value="USD" selected>US Dollar</option>
                <option value="BWP">Botswana Pula</option>
                <option value="ZAR">South African Rand</option>
                </select></div>
                <div class="col-md-12">
                <h2 id="bwp_amount"></h2>
                <label class="control-label">Additional note for us (optional)</label>
                <textarea class="form-control" name="m_2"></textarea>

                </div>
                <input name="p1" type="hidden" value="'.KVI_VCS_MERCHANT_ID.'" />
                <input id="p4" name="p4" type="hidden" value="0" />
                <input name="p3" type="hidden" value="Online Payment For Booking Made" />
                <input name="p10" type="hidden" value="'.$current_url.'" />
                <input name="UrlsProvided" type="hidden" value="Y" />
                <input name="ApprovedUrl" type="hidden" value="'.$approved_url.'" />
                <input name="DeclinedUrl" type="hidden" value="'.$declined_url.'" />

                <div class="col-md-12 text-center buttons-container hidden" id="submit-div">
                    <button class="moon-button medium btn-book-via-email" role="submit">Proceed To Payment</button>
                </div>
                </form> ';  
            } else {
                $out = '<h3 class="text-danger">VCS configuration options are not set. Please contact us and tell us about this error';
            }     
        return $out;
}
/*
Register shorcodes
*/
function kvi_register_shortcodes(){
	// Registered Shortcodes
	add_shortcode ('kvi_vcs_form', 'kvi_vcs_form_display' );
};