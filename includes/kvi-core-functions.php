<?php
add_filter( 'plugin_action_links_'.KVI_PLUGIN_BASENAME, 'plugin_add_settings_link' );
add_action( 'admin_menu', 'kvi_settings_page_create' );

function plugin_add_settings_link( $links ) {
    $settings_link = '<a href="options-general.php?page=kvi_vcs_settings">' . __( 'Settings' ) . '</a>';
    array_push( $links, $settings_link );
  	return $links;
}

function kvi_settings_page_create() {
    $page_title = 'Kajuzi VCS Settings';
    $menu_title = 'VCS Settings';
    $capability = 'manage_options';
    $menu_slug = 'kvi_vcs_settings';
    $function = 'kvi_vcs_display_settings_page';
    $icon_url = '';
    $position = 24;

    add_options_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
}


function kvi_vcs_display_settings_page()
{
    global $wp;
    
    // Check that the user is allowed to update options
    if (!current_user_can('manage_options')) {
        wp_die('You do not have sufficient permissions to access this page.');
    }

    if (isset($_POST['currencylayer_api_key'])) {
        $currencylayer_api_key = $_POST['currencylayer_api_key'];
        update_option('kvi_currencylayer_api_key', $currencylayer_api_key);
    }

    if (isset($_POST['vcs_merchant_id'])) {
        $vcs_merchant_id = $_POST['vcs_merchant_id'];
        update_option('kvi_vcs_merchant_id', $vcs_merchant_id);
    }

    if (isset($_POST['approved_url'])) {
        $approved_url = $_POST['approved_url'];
        update_option('kvi_approved_url', $approved_url);
    }

    if (isset($_POST['declined_url'])) {
        $declined_url = $_POST['declined_url'];
        update_option('kvi_declined_url', $declined_url);
    }

    $currencylayer_api_key = get_option('kvi_currencylayer_api_key', '');
    $vcs_merchant_id = get_option('kvi_vcs_merchant_id', '');
    $approved_url = get_option('kvi_approved_url', '');
    $declined_url = get_option('kvi_declined_url', '');
    $rates = json_decode(get_option('kvi_currencylayer_quotes'));
    $last_update = get_option('kvi_currencylayer_last_update');
?>
        <div class="wrap">
            <h2>Exchange Rates</h2>
            <?php if(isset($rates->USDBWP)): ?>
            <h3>The current rates are:</h3>
            <p><strong>USD-BWP:</strong> <?php echo $rates->USDBWP ?> &nbsp; <strong>ZAR-BWP:</strong> <?php echo round($rates->USDBWP / $rates->USDZAR, 6)  ?></p>
            <h3>Rates last updated <?php echo date('d M Y H:m:s', $last_update) ?></h3>
            <?php else: ?>
            <h3>No rates have been stored yet</h3>
            <p>Make sure your settings are correct and have been saved</p>
            <?php endif ?>
            
            <hr>
            <h2>VCS Settings</h2>
            <form method="POST">
                <table class="form-table">
                    <tr>
                        <th scope="row">VCS Merchant ID</th>
                        <td>
                            <input type="text" class="regular-text" name="vcs_merchant_id" id="vcs_merchant_id" value="<?php echo $vcs_merchant_id; ?>"><br>
                            <span class="description">Obtain this from VCS</span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Currency Layer API Key</th>
                        <td>
                            <input type="text" class="regular-text" name="currencylayer_api_key" id="currencylayer_api_key" value="<?php echo $currencylayer_api_key; ?>"><br>
                            <span class="description">Obtain this from <a href="https://currencylayer.com/product" target="_blank" rel="noopener noreferrer">CurrencyLayer</a></span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Approved URL (optional)</th>
                        <td>
                            <input type="text" class="regular-text" name="approved_url" id="approved_url" value="<?php echo $approved_url; ?>"> <br>
                            <span class="description">
                                Where you want your visitors to be redirected to after successfully paying on the VCS platform<br>
                                Defaults to the url of the page that contains the form
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Declined URL (optional)</th>
                        <td>
                            <input type="text" class="regular-text" name="declined_url" id="declined_url" value="<?php echo $declined_url; ?>"><br>
                            <span class="description">
                                Where you want your visitors to be redirected when the transaction fails<br>
                                Defaults to the url of the page that contains the form
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"></th>
                        <td>
                            <input type="submit" value="Save" class="button button-primary button-large">
                        </td>
                    </tr>
                </table>                
            </form>
        </div>
    <?php
}


function kvi_notify_admins()
{
    $admins = get_users(array('role' => 'administrator'));
    $to = array();
    foreach ($admins as $admin) {
        $to[] = $admin->user_email;
    }
	$subject = '[ERROR] Exchange failed to update';
	$message = 'The VCS plugin failed to update the forex rate. Please attend to this problem';
	$headers = array('Content-Type: text/html; charset=UTF-8');
	wp_mail( $to, $subject, $message, $headers );
}
?>