<?php 
/*
*	Methods for AJAX requests	
*/

if ( ! defined( 'WPINC' ) ) {die;}

add_action( 'wp_ajax_get_currency_rates', 'kvi_currency_rates' );
add_action( 'wp_ajax_nopriv_get_currency_rates', 'kvi_currency_rates' );
add_shortcode( 'kvi_vcs_response', 'kvi_vcs_response' );

function kvi_currency_rates(){
    /* Update forex rates in the database */
    $quotes = get_option('kvi_currencylayer_quotes', null);
    $quotes_age = time() - get_option('kvi_currencylayer_last_update');
	if (!$quotes || $quotes_age > 43200) { 
		$json = file_get_contents(KVI_EXCHANGE_RATE_API_ENDPOINT);
        $cl_obj = json_decode($json);
        
        if (!$cl_obj->success){
            kvi_notify_admins();
            return wp_send_json_error($cl_obj->error);
        }
		update_option('kvi_currencylayer_last_update', time());
		update_option('kvi_currencylayer_quotes', json_encode($cl_obj->quotes));
    }
    $lastupdate = get_option('kvi_currencylayer_last_update');
	$data = array(
		"quotes_age"=> time() - $lastupdate,
		"quotes" => json_decode(get_option('kvi_currencylayer_quotes'))
	);
	return wp_send_json_success($data);
}

// VCS response shortcode
function kvi_vcs_response() {
	if (!isset( $_POST['p2'] )){
		return '<h2 style="text-align: center;">Unfortunately this page did not receive information to show that your transaction was successful.</h2>	<h3 style="text-align: center;">If you have just made a payment, please get in touch with us and check if we have received the funds</h3>';
	} elseif ( $_POST['p4'] == 'Duplicate' ) {
		return '<h2 style="text-align: center;">Duplicate transaction detected</h2>	<h3 style="text-align: center;">Transaction for Ref: <strong>'.$_POST['p2'].'</strong> already exists in our records. Please obtain a new reference number for a new transaction</h3>';
	} elseif ( strpos( $_POST['p3'], 'APPROVED' ) ) {
		return '<h2 class="open-sans" style="text-align: center;">Payment Successful!!!</h2>
		<p style="text-align: center;"><strong>Please note that because our bank charges in Pula there may be a slight difference between what we invoiced you for and what will be charged on your card.
		</strong></p>
		<p style="text-align: center;">Your card is going to be charged for '.($_POST['m_5'] != 'BWP' )?'the '.$_POST['m_5'].' equivalent of ':''.'BWP '.$_POST['p9'].'. Details of this transaction have been sent to <em><strong>'.$_POST['p9'].'</strong></em>.</p>

		<h3 style="text-align: center;">Thank you for making our day with this payment!</h3>';
	} else {
		return '<h2 style="text-align: center;">Error:'. $_POST['p3'] .'</h2>	<h3 style="text-align: center;">Something went wrong with you transaction. Please send us an email to <strong>reservations@chobebakwena.com</strong> with details of this error</h3>';
	}
}