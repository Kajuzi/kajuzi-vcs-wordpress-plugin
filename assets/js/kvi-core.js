(function ($, window, undefined) {
    $(document).ready(function () {

        currencyRatesData = {};

        var currencies = [];

        $.ajax({
            url: "../wp-admin/admin-ajax.php",
            type: 'POST',
            crossDomain: true,
            data: {
                action: "get_currency_rates"
            },
            success: function (response) {
                if(response.success == true) {
                    currencyRatesData = response.data;
                    currencies['ZAR'] = currencyRatesData.quotes.USDBWP / currencyRatesData.quotes.USDZAR;
                    currencies['BWP'] = 1;
                    currencies['USD'] = currencyRatesData.quotes.USDBWP;

                    activate_submit();
                } else {
                    rates_error();
                }

            },
            error: function (error) {
                console.log(error);
                rates_error();
            }
        });

        if ($("#kvi-vcs-payment-form").length) {

            var currency = $('#currency').val();
            var amount = $('#amount').val();

            $('#amount').on('change', function () {
                amount_changed(this);
            });

            $('#amount').keyup(function () {
                amount_changed(this);
            });

            $('#currency').on('change', function () {
                currency = this.value;
                calc_bwp();
            });

            function amount_changed(input) {
                old_amount = amount;
                amount = input.value;
                if (!$.isNumeric(parseInt(amount))) {
                    $('#bwp_amount').html('Please enter a valid number')
                    $('#amount').val(old_amount);
                } else if (amount > 50000) {
                    $('#bwp_amount').html('Amount too large')
                    $('#amount').val(old_amount);
                } else {
                    calc_bwp();
                }
            }

            function calc_bwp() {
                bwp_amount = currencies[currency] * amount;
                amount = amount?amount:0;
                $('#p4').val(bwp_amount.toFixed(2));
                if (currency == 'BWP')
                    $('#bwp_amount').html('&nbsp;')
                else
                    $('#bwp_amount').html(amount + ' ' + currency + ' is ' + bwp_amount.toFixed(2) + ' Pula')
            }

            function clear_message() {
                $('#bwp_amount').html(' ');
                $('#bwp_amount').removeClass('text-danger'); 
            }

            function activate_submit() {
                $('#submit-div').removeClass('hidden');
            }

            function rates_error() {
                $('#bwp_amount').html('Something went wrong trying to get current exchange rates. Please contact us');
                $('#bwp_amount').addClass('text-danger');  
                $('#kvi-vcs-payment-form input').prop('disabled', true);
            }

        };

    }); /* end of document.ready */
})(jQuery, window);